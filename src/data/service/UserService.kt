package com.jalalkun.firstapi.data.service

import com.jalalkun.firstapi.model.Profile

interface UserService {
    fun registerUser(name: String, username: String, email: String, password: String): Int
    fun getAllUsers(): MutableList<Profile>
    fun validateLogin(username: String, password: String): Boolean
}
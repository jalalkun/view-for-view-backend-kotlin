package com.jalalkun.firstapi.data.service

import com.jalalkun.firstapi.model.Note


interface NotesService {
    fun addNote(note: String): Int
    fun deleteNote(id: Int): Int
    fun updateNote(id: Int, note: String)
    fun fetchAllNotes(): List<Note>
    fun fetchNoteWithId(id: Int): Note?
}
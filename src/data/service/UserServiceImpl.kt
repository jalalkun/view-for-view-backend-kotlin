package com.jalalkun.firstapi.data.service

import com.jalalkun.firstapi.data.repository.UserRepository
import com.jalalkun.firstapi.model.Profile

class UserServiceImpl(private val userRepository: UserRepository): UserService {
    override fun registerUser(name: String, username: String, email: String, password: String): Int {
        return userRepository.registerUser(name, username, email, password)
    }

    override fun getAllUsers(): MutableList<Profile> {
        TODO("Not yet implemented")
    }

    override fun validateLogin(username: String, password: String): Boolean {
        return userRepository.validateLogin(username, password)
    }
}
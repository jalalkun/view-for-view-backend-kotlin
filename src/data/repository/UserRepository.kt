package com.jalalkun.firstapi.data.repository

import com.google.gson.Gson
import com.jalalkun.firstapi.data.db.DatabaseConnection
import com.jalalkun.firstapi.data.entity.UserEntity
import com.jalalkun.firstapi.utils.Utils.encryptSha512
import org.ktorm.dsl.*

class UserRepository {
    private val db = DatabaseConnection.database
    fun registerUser(name: String, username: String, email: String, password: String): Int{
        val result = db.insert(UserEntity){
            set(it.name, name)
            set(it.username, username)
            set(it.email, email)
            set(it.password, password.encryptSha512())
        }
        return result
    }

    private fun getUserValidate(username: String): UserValidate {
        val result = UserValidate()
        val query = db.from(UserEntity).select(UserEntity.username, UserEntity.password).where{
            (UserEntity.username eq username)
        }
        query.map {
            result.username = it[UserEntity.username]
            result.password = it[UserEntity.password]
        }
        return result
    }

    fun validateLogin(username: String, password: String): Boolean{
        val userValidate = UserRepository().getUserValidate(username)
        var result = false
        println("user-validate ${Gson().toJson(userValidate)}")
        userValidate.let {
            if (it.username == username && it.password == password.encryptSha512()){
                result = true
            }
            return result
        }
    }
}

data class UserValidate(
    var username: String? = null,
    var password: String? = null
)
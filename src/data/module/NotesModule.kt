package com.jalalkun.firstapi.data.module

import com.jalalkun.firstapi.data.service.NotesService
import com.jalalkun.firstapi.data.service.NotesServiceImpl
import com.jalalkun.firstapi.data.repository.NotesRepository
import org.koin.dsl.module

val notesModule = module {
    single<NotesService> { NotesServiceImpl(get()) } // get() Will resolve HelloRepository
    single { NotesRepository() }
}
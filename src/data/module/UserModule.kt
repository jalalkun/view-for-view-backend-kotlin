package com.jalalkun.firstapi.data.module

import com.jalalkun.firstapi.data.repository.UserRepository
import com.jalalkun.firstapi.data.service.UserService
import com.jalalkun.firstapi.data.service.UserServiceImpl
import org.koin.dsl.module

val userModule = module {
    single<UserService> { UserServiceImpl(get()) }
    single { UserRepository() }
}
package com.jalalkun.firstapi.data.db

import org.ktorm.database.Database
import org.ktorm.logging.ConsoleLogger
import org.ktorm.logging.LogLevel

object DatabaseConnection {
    val database = Database.connect(
        url = "jdbc:mysql://localhost:3306/view_for_view_db",
        driver = "com.mysql.cj.jdbc.Driver",
        user = "view_view_admin",
        password = "K(gNS7bZDwi2]CNt",
        logger = ConsoleLogger(threshold = LogLevel.DEBUG)
    )
}
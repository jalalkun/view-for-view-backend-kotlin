package com.jalalkun.firstapi.serialization

import com.jalalkun.firstapi.data.module.notesModule
import com.jalalkun.firstapi.data.module.userModule
import com.jalalkun.firstapi.route.notesRoutes
import io.ktor.application.*
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.modules

fun Application.configureKoin() {
    install(Koin) {
        modules(userModule)
        modules(notesModule)
    }
}
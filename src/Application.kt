package com.jalalkun.firstapi

import com.jalalkun.firstapi.route.configureRoute
import com.jalalkun.firstapi.serialization.configureKoin
import com.jalalkun.firstapi.serialization.configureMonitoring
import com.jalalkun.firstapi.serialization.configureSerialization
import io.ktor.application.*

fun main(args: Array<String>): Unit = io.ktor.server.cio.EngineMain.main(args)

@Suppress("unused")
fun Application.module() {
    configureKoin()
    configureSerialization()
    configureMonitoring()
    configureRoute()
}


package com.jalalkun.firstapi.route.user

import com.jalalkun.firstapi.data.service.UserService
import com.jalalkun.firstapi.model.BaseResponse
import com.jalalkun.firstapi.route.emptyParameter
import com.jalalkun.firstapi.route.missingParameter
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Application.userRouting() {
    val path = "/auth"
    val userService: UserService by inject()
    routing {
        route("$path/register") {
            post {
                val params = call.receiveParameters()
                val name = params["name"] ?: return@post call.respond(HttpStatusCode.BadRequest, missingParameter())
                val username = params["username"] ?: return@post call.respond(HttpStatusCode.BadRequest, missingParameter())
                val email = params["email"] ?: return@post call.respond(HttpStatusCode.BadRequest, missingParameter())
                val password = params["password"] ?: return@post call.respond(HttpStatusCode.BadRequest, missingParameter())

                val kosong = arrayListOf<String>()
                if (name.isEmpty()) kosong.add("Name")
                if (username.isEmpty()) kosong.add("Username")
                if (email.isEmpty()) kosong.add("email")
                if (password.isEmpty()) kosong.add("password")
                if (kosong.isNotEmpty()) {
                    var message = "Tidak boleh kosong: "
                    kosong.forEach {
                        message += "$it "
                    }
                    call.respond(HttpStatusCode.BadRequest, BaseResponse(HttpStatusCode.BadRequest.value, message))
                    return@post
                }
                println("auth register above result")
                val result = userService.registerUser(
                    name,
                    username,
                    email,
                    password
                )
                println("auth register result $result")
                if (result == 1) call.respond(
                    HttpStatusCode.Created,
                    BaseResponse(HttpStatusCode.OK.value, "Berhasil register user")
                )
                else call.respond(
                    HttpStatusCode.BadRequest,
                    BaseResponse(HttpStatusCode.BadRequest.value, "Gagal register user")
                )
            }
        }
        route("$path/login"){
            post {
                val params = call.receiveParameters()
                val username = params["username"] ?: return@post call.respond(HttpStatusCode.BadRequest, missingParameter())
                val password = params["password"] ?: return@post call.respond(HttpStatusCode.BadRequest, missingParameter())
                if (username.isEmpty() || password.isEmpty()) return@post call.respond(HttpStatusCode.BadRequest, emptyParameter())
                val result = userService.validateLogin(username, password)
                if (result) call.respond(HttpStatusCode.OK, mapOf(
                    "status" to HttpStatusCode.OK.value.toString(),
                    "message" to "Berhasil login"
                ))
                else call.respond(HttpStatusCode.BadRequest, mapOf(
                    "status" to HttpStatusCode.BadRequest.value.toString(),
                    "message" to "Username dan password tidak cocok"
                ))
            }
        }
    }
}

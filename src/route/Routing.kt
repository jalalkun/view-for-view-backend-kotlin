package com.jalalkun.firstapi.route

import com.jalalkun.firstapi.model.Profile
import com.jalalkun.firstapi.route.user.userRouting
import com.jalalkun.firstapi.utils.Utils.encryptSha512
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.serialization.Serializable

val profiles = mutableListOf<Profile>()

fun Application.configureRoute(){
    routing {
        userRouting()
        notesRoutes()
    }
    routing {
        route("/"){
            get {
                call.respond(HttpStatusCode.OK, mapOf(
                    "status" to "oke",
                    "message" to "berhasil mengakses api"
                ))
            }
        }
        route("/testEncrypt"){
            post {
                val value = call.receiveParameters()["value"] ?: return@post call.respond(HttpStatusCode.BadRequest, "missing value")
                call.respond(HttpStatusCode.OK, mapOf(
                    "result" to value.encryptSha512()
                ))
            }
        }
    }
}

fun emptyParameter() = mapOf(
    "status" to HttpStatusCode.BadRequest.value.toString(),
    "message" to "Empty Parameter"
)

fun missingParameter() = mapOf(
    "status" to HttpStatusCode.BadRequest.value.toString(),
    "message" to "Missing Parameter"
)
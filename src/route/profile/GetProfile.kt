package com.jalalkun.firstapi.route.profile

import com.jalalkun.firstapi.model.Profile
import com.jalalkun.firstapi.route.profiles
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.serialization.Serializable

fun Route.getProfiles() {
    get {
        if (profiles.isNotEmpty())
            call.respond(
                HttpStatusCode.OK, ResponProfiles(
                    status = HttpStatusCode.OK.value,
                    message = "Berhasil mendapatkan profile",
                    data = profiles
                )
            )
        else
            call.respond(HttpStatusCode.NotFound, "profile kosong")
    }
}

@Serializable
data class ResponProfiles(
    val status: Int,
    val message: String,
    val data: MutableList<Profile>
)
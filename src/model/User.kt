package com.jalalkun.firstapi.model

data class User(
    val name: String,
    val username: String,
    val email: String
)
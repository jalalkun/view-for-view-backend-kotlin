package com.jalalkun.firstapi.model

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class Profile(
    @SerializedName("id")
    var id: String,
    @SerializedName("name")
    var name: String
)
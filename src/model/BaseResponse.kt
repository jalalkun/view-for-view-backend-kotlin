package com.jalalkun.firstapi.model

import kotlinx.serialization.Serializable

@Serializable
data class BaseResponse(
    val status : Int,
    val message: String
)